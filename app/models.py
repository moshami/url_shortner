#!/usr/bin python3

# Imports
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# # 3rd party:
from django.db import models
from django.utils.translation import gettext_lazy as _

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class UrlObject(models.Model):
    """
    Url model definitions
    """
    original = models.CharField(
        verbose_name=_('Original URL'),
        max_length=254,
    )
    shorten = models.CharField(
        verbose_name=_('Shorten URL'),
        max_length=16,
        unique=True,
        blank=True,
        null=True,
    )
    created = models.DateTimeField(
        verbose_name=_('Creation date/time'),
        editable=False,
        auto_now_add=True
    )

    class Meta:
        """ Metadata """
        app_label = 'app'
        verbose_name = _('Url Object')
        verbose_name_plural = _('Url Objects')

    def __str__(self):
        """
        String representation for Url object
        :return: pk
        """
        return "Shorten url for: {original} is {shorten}".format(
            original=self.original,
            shorten=self.shorten
        )
