#!/usr/bin python3

# Imports
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 3rd party:
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

# Internal:
from app.models import UrlObject

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


@admin.register(UrlObject)
class UrlObjectAdmin(admin.ModelAdmin):
    """
    Admin class for the UrlObject model.
    """
    list_display = (
        'original',
        'shorten',
        'created',
    )
    search_fields = (
        'pk',
        'original',
        'shorten',
    )
    list_filter = (
        'created',
    )
    fieldsets = (
        (_('Urls'), {
            'fields': (
                'original',
                'shorten',
            ),
        }),
        (_('Timestamp'), {
            'fields': (
                'created',
            ),
        }),
    )
    readonly_fields = (
        'created',
    )
    date_hierarchy = 'created'
    list_per_page = 24
    save_on_top = True
