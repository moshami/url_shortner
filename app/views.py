#!/usr/bin python3

# Imports
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# # 3rd party:
import random
import string
from django.shortcuts import render, redirect, get_object_or_404
from django.views import View

# Internal:
from app.forms import ShortenUrlForm
from app.models import UrlObject
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ShortenUrlCreateView(View):
    """
    ShortenUrlCreateView class view to create a ShortenUrl
    """
    form_class = ShortenUrlForm
    template_name = 'app/home.html'

    def get(self, request, *args, **kwargs):
        """
        ShortenUrlCreateView get method
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context = {
            'form': self.form_class
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        """
        ShortenUrlCreateView post method
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = self.form_class(request.POST)
        if form.is_valid():
            shorten_url = ''.join(
                random.choices(string.ascii_letters + string.digits, k=16)
            )
            original_url = form.cleaned_data['original_url']
            new_url = UrlObject()
            new_url.original = original_url
            new_url.shorten = shorten_url
            new_url.save()
            context = {
                'form': form,
                'shorten_url': new_url.shorten
            }

            return render(request, self.template_name, context)
        else:
            context = {
                'form': self.form_class
            }

            return render(request, self.template_name, context)


class ShortenUrlGetView(View):
    """
    ShortenUrlGetView class view to get a original url for a shorten url
    """

    def get(self, request, *args, **kwargs):
        """
        ShortenUrlGetView get method
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        url = get_object_or_404(
            UrlObject,
            shorten=kwargs['shorten_url']
        ).original

        return redirect(url)
