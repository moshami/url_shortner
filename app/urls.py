from django.urls import path
from . import views

urlpatterns = [
    path('',
         views.ShortenUrlCreateView.as_view(),
         name='create-shortener-url'),
    path('get-url/<str:shorten_url>/',
         views.ShortenUrlGetView.as_view(),
         name='get-shortener-url'),
    ]

