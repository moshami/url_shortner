

### Description - Admin ###


in this challenge, we're asking you to create your very own URL shortener that runs on a container! The URL shorteners can be used to reduce any long and unwieldy website address can be to just a few characters with the click of a button. Some popular examples are Bitly, Tiny URL, Tiny CC, Bit DO, and others



### How do I get set up? ###

- go to project [repo site](https://bitbucket.org/moshami/url_shortner/src/master/)
- clone source code in your home directory
- `git clone git@bitbucket.org:moshami/url_shortner.git`
- go to your terminal and `cd` into your project directory
- to build and run images `docker-compose up`
- to create createsuperuser in your terminal, create a separate tab and use
`docker-compose exec web python manage.py createsuperuser`
- go to your browser and use `http://0.0.0.0:8001/` url to load your site locally


### The below haven't been done yet ###

* Unit tests - test
* Good frontend components
