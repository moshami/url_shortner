FROM python:3.7-slim

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /url_shortner_code
COPY Pipfile Pipfile.lock /url_shortner_code/

RUN apt-get update
RUN apt-get install -y postgresql-server-dev-11 gcc python3-dev musl-dev
RUN apt-get install -y postgresql-client


RUN pip3 install pipenv
RUN pipenv install --system

COPY . /url_shortner_code/